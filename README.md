![Screenshot preview of the theme "Calamity" by glenthemes](https://64.media.tumblr.com/0f71f1db45c627d92912839806792707/e58b18a813b5c193-4b/s1280x1920/48be9bd25b4e61b3ee8c6e6ff25565edf56a2b79.png)

**Theme no.:** 18  
**Theme name:** Calamity  
**Theme type:** Free / Tumblr use  
**Description:** Previously called ‘Hybrid’, this theme has been revamped into ‘Calamity’, a dual sidebar theme featuring Vergil ~~the storm that is approaching~~ from Devil May Cry.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2016-06-11](https://64.media.tumblr.com/33d001313ed9bbd8b59a93e080d196b8/tumblr_o8n3ihWf811ubolzro1_540.gif)  
**Rework date:** 2021-01-28

**Post:** [glenthemes.tumblr.com/post/641574572661014529](https://glenthemes.tumblr.com/post/641574572661014529)  
**Preview:** [glenthpvs.tumblr.com/calamity](https://glenthpvs.tumblr.com/calamity)  
**Download:** [pastebin.com/zUWchPVP](https://pastebin.com/zUWchPVP)  
**Guide:** [glenthemes.tumblr.com/calamity-help](https://glenthemes.tumblr.com/calamity-help)  
**Credits:** [https://glenthemes.tumblr.com/calamity-crd](https://glenthemes.tumblr.com/calamity-crd)
