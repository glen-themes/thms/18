// nothing to see here but bad coding etiquette

$(document).ready(function(){
    // tumblr bs
    $(".tumblr_preview_marker___").remove();
    $(".tumblr_theme_marker_blogtitle").contents().unwrap();

    var getbg = getComputedStyle(document.documentElement)
               .getPropertyValue("--Sidebar-Background");

    $(".rightside").attr("bg",getbg);

    // if sb image is vergil on install
    var daddyissues = $(".vergin").attr('src');
    if(daddyissues.indexOf('64.media.tumblr.com/85d74581f1237978faff9da1b6f9c4bb/44e2b3171eb8c527-a9/s540x810/c08aca1c1b153a21093bb6fffa85edb3eb62ef50.gif') >=0){
        if($(".rightside").attr("bg") == "#040405"){
            $(".vergin").css({
                "margin-top":"-30px",
                "user-select":"none",
            });
        }
    }

    // remove blog name on postnotes tooltips
    $("ol.notes").find("a").removeAttr("title");

    // center text on 'not found' page
    $("h1.title").each(function(){
        if($(this).text() == "Not Found"){
            if($(this).next(".body").find("p:first").text() == "The URL you requested could not be found."){
                $(this).next(".body").css({"text-align":"center","margin-top":"var(--Heading-Margins)"})
            }
        }
    });

    // load 'reblog', 'like', & glen logo SVGs
    fetch("//glen-assets.github.io/core-svgs/freepik_repeat.html")
    .then(rt_svg => {
      return rt_svg.text()
    })
    .then(rt_svg => {
      $(".retweet").html(rt_svg)
    });

    fetch("//glen-assets.github.io/core-svgs/freepik_like.html")
    .then(like_svg => {
      return like_svg.text()
    })
    .then(like_svg => {
      $(".heart").html(like_svg)
    });

    fetch("//glen-assets.github.io/core-svgs/glenjamin.html")
    .then(glenjamin => {
      return glenjamin.text()
    })
    .then(glenjamin => {
      $(".glenjamin").html(glenjamin)
    });

    /*------------------------------------------*/
    $(".boxtext").each(function(){
        if($(this).children().first().is("li")){
            $(this).find("li").wrapAll("<ul>")
        }
    });

    $(".boxtext").each(function(){
        if($(this).text().startsWith("*")){
            $(this).find("a[href]").each(function(){
                var geturl = "[" + $(this).attr("href") + "]";
                var linktxt = "{" + $(this).text() + "}";
                $(this).text(linktxt + geturl);

                $($(this).get(0).childNodes[0] ).unwrap();
            });
        }
    });

    $(".boxtext").each(function(){
        if($(this).text().startsWith("*")){
            $(this).html(
                $(this).html()
                    .replaceAll('*', '<span class="dot">*</span>')
                    .replaceAll("<b>","+strong+")
                    .replaceAll("</b>","-strong-")
                    .replaceAll("<i>","+italic+")
                    .replaceAll("</i>","-italic-")
            );

            $(this).contents().filter(function(){
                return this.nodeType === 3;
            }).wrap('<div class="l-row"/>');

            $(this).find(".dot").remove();
        }//end if-asterisk
    });

    $(".boxtext br").each(function(){
        if($(this).closest(".boxtext").length){
            $(this).remove();
        }
    });

    $(".l-row").each(function(){
        var txt = $(this).html();

        var front = txt.substr(0, txt.indexOf(':')),
            last = txt.split(":").pop().trim();

        $(this).html("<div class='holly'>" + front + ":</div>" + "<div class='wood'>" + last + "</div>")
    });

    $(".holly, .wood").each(function(){
         $(this).html(
            $(this).html()
                .replaceAll('+strong+', '<b>')
                .replaceAll('-strong-', '</b>')
                .replaceAll('+italic+', '<i>')
                .replaceAll('-italic-', '</i>')


        );
    });

    $(".holly").each(function(){
        if($(this).text().startsWith(":")){
            $(this).parent().remove();
        }
    });

    $(".wood").each(function(){
        var tt = $(this).text();
        if(tt.indexOf("{") != -1){
            if(tt.indexOf("}") != -1){
                var def = tt.substr(0, tt.indexOf('{'));

                var getlinktxt = tt.split('{').pop().split('}')[0];
                var getlinkurl = tt.split('[').pop().split(']')[0];

                var aff = tt.split("]").pop();

                $(this).html(def + '<a href="' + getlinkurl + '">' + getlinktxt + '</a>' + aff)
            }
        }
    });

    /*------------------------------------------*/

    // if:reblog dividers, increase space between 1st commenter & the next
    var p_gap = getComputedStyle(document.documentElement)
               .getPropertyValue("--Paragraph-Margins"),
        numonly = parseFloat(p_gap),
        suffix = p_gap.split(/\d/).pop();

    $(".oui p").each(function(){
        if($(this).parents(".comment_container").next().length){
            $(this).css("margin-bottom",numonly / 2 + suffix)
        }
    });

    // add space to 'read more' | if reblog dividers are ON
    if($(".posts").hasClass("oui")){
        $("a.read_more").each(function(){
            if(!$(this).parent().is("[style]")){
                $(this).css("padding-bottom","var(--Paragraph-Margins)")
            }
        });
    }


    $(".oui").each(function(){
        // remove extra space from first commenter
        // if there is nothing before it
        if(!$(this).find(".comment_container:first").closest(".body").length){
            $(this).find(".comment_container:first").addClass("ccfirst")

        };

        // remove reblog border from last commenter
        $(this).find(".comment_container:last").addClass("cclast");
    });



    // turn npf_chat into a chat post
    $(".npf_chat").addClass("chatholder").removeClass("npf_chat");

    $(".chatholder").each(function(){
        if($(this).attr("data-npf")){
            $(this).find("b").addClass("chat_label");
            $(this).contents().last().wrap("<span class='chat_content'>")
        }
    });
    // add colon at the end of each chat_label
    $(".chat_label").each(function(){
        if(!$(this).text().endsWith(":")){
            $(this).append(":")
        }
    });

    $(".posts").each(function(){
        // show source if post does not have caption
        if(!$(this).find(".comment_container").length){
            $(this).find(".nosrc").show()
        }
    });

    $(".nosrc").each(function(){
        if($(this).is(":hidden")){
            $(this).remove()
        }
    });

    $(".body").each(function(){
        if(!$(this).prev().length){
            var cf = $(this).find(".comment_container:first");
            cf.css("margin-top","calc(var(--Captions-Gap) / -2)");
            if(cf.prev().is(".boooob")){
                if(cf.is("[style]")){
                    cf.css("margin-top","")
                }
            }
        }
    });


    $(".npf_inst").each(function(){
        if($(this).parent().prev(".commenter")){
            $(this).css("margin-top","var(--NPF-Caption-Spacing)")
        }
    });


    $(".posts").each(function(){

        var fc = $(this).find(".comment_container").eq(0);
        var fcinst = fc.find(".npf_inst");
        if(fcinst.length){
            if(!fcinst.prev().length){
                if(fcinst.next().length){
                    fcinst.prependTo($(this));
                    fcinst.css("margin-bottom","var(--NPF-Caption-Spacing)")
                    fc.css("margin-top","")
                }
            }
        }

        if($(this).children().first().is(".npf_inst")){
            $(this).children().first().css("margin-top","")
        }
    });


    $(".caption").each(function(){
        // remove empty captions
        if(!$(this).children().length){
            $(this).remove();
        }
    });

	/*
    $(".posts br").each(function(){
        // remove unnecessary <br>s
        if(!$(this).prev().length){
            $(this).remove();
        }
    });
	*/

    $(".ccfirst").each(function(){
        if(!$(this).closest(".caption").length){
            if(!$(this).prev().length){
                $(this).css("margin-top","calc(var(--Captions-Gap) / -2)")
            }
        }
    });

	$(".cclast").each(function(){
		if(!$(this).prev(".comment_container").length){
			if($(this).prev().is(".npf_inst")){
				$(this).css("margin-top","")
			}
		}
	});

});//end ready
